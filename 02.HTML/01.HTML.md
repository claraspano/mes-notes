# Les bases HTML

## Sommaire

1. [Liens](#liensHtml)
2. [Les listes](#listes)
3. [Les liens](#lesLiens)


------
<a id="liensHtml"></a>
Liens des videos : 
- [Les bases du HTML] (https://www.youtube.com/watch?v=tXBF1el1yfA&list=PLDeebGAUwdpHqeKK5wm8X77K1dCBs0tLM&index=1)
------

1 - Créer un fichier HTML 'index'
2 - Mettre la langue en FR
3 - Titre : Ce qui apparait sur l'onglet

° Fonctionne avec des balises ouvrantes et fermantes

>Head = Données sur la page mais qui sont invisible sur la page

>Body = Corps de la page 
 -  Composé du - Header : <header></header>
               - Corps : <div></div>
               - Footer : <footer></footer>
               
 >Header = Haut de la page
 > Footer : Bas de page, copyright, liens de bas de page etc

*Le header et le footer sont à placer dans le body*

> Ajouter une image 
 - Créer un dossier Assets
 - Copier l'image dedans
 - Dans le code HTML ecrire
   - <img src=assets/nomdufichier" alt="description de l'image"> 

> P - est une balise paragraphe

> H - est une balise titre, peut aller jusqu'à h6

<a id="listes"></a>

### Les listes

- Les listes non-ordonnées : La balise utilisée est <ul>
- Les listes ordonnéess : La balise utilisée est <ol> 
- Chaque élément d'une liste est balisé avec un élément <li>

exp :
 `<ul>
         <li>technologues</li>
         <li>chercheurs</li>
         <li>bâtisseurs</li>
       </ul>
`

<a id="lesLiens"></a>

### Les liens 

- Encadrez le texte dans un élément <a>
- Mettre un attribut href pour l'élément <a>
- Dans cet attribut, ajoutez le lien vers le site

** On peut mettre des elements de style dans des balise html **

