# 💠 Clean architecture

## 🔗 Lien vidéo

1️⃣ Clean architecture (https://www.youtube.com/watch?v=r_FQLW5DFfs)

## 🔗 Liens utiles 

1️⃣ Lien git Chalom : (https://gitlab.com/chalom.ellezam/clean-architecture.git)
2️⃣ Doc : (https://medium.com/@bitsydarel/clean-architecture-with-design-pattern-from-darel-perspective-part-1-domain-layer-e1ed7bb2bd21#:~:text=Clean%20architecture%20is%20a%20way,each%20layer%20maintains%20its%20code)
3️⃣ Doc Usecase : (http://www.plainionist.net/Implementing-Clean-Architecture-UseCases/)
4️⃣ Doc : (https://www.kaherecode.com/tutorial/comprendre-la-clean-architecture)

------------

# 💠 Clean architecture

#### Qu'est-ce que la clean architecture ?

▪️ **La clean architecture** est un moyen de _structurer_ ou de _séparer_ votre code de sorte que ce que fait votre application , comment votre application fait ce qu'elle fait et comment votre application affiche ce qu'il a fait , soient _séparés_ et ne _dépendent_ pas les uns des autres.

▪️ **La clean architecture** vous offre les avantages suivants:
   - **Maintenabilité** (chaque couche conserve son code)
   - **Testabilité** (chaque couche teste son code)
   - **Modularité** (chaque couche peut être commutée)

![img.png](00.assets/cla1.png)
![img.png](00.assets/cla2.png)

▪️ La **clean architecture** est une _organisation en couche_, chaque _couche_ ayant des responsabilités bien définies.

-------------

# 💠 UseCase

#### Qu'est ce qu'un UseCase

▪️ **Un Usecase** est une **liste d'actions** ou **d'étapes d'événement** définissant généralement les **interactions** entre un **acteur** et un **système** pour _atteindre un objectif_.

▪ ️Ces **Usecases** orchestrent le **flux de données** vers et à partir des **entités**, et ordonnent à ces **entités** d'utiliser leurs règles métier critiques pour atteindre les objectifs du **Usecase**.

▪️ Un **Usecase** est une **fonctionnalité métier**. C'est dans cette _couche_ qu'on **implémente** les _fonctionnalités métiers_ de l'application.

📚 Exp :

▪️ Dans le Core/Domain/Model le fichier interface des Usecases
```typescript

//<TParams, TResponse>
//Cela s'appelle des "Typage Generique".
//Dans mon interface Usecase, je ne connais pas les type à l'avance, alors je met un Typage Generique
//pour pouvoir mettre ce que je souhaite


export interface Usecase<TParams, TResponse> {
    execute(request?: TParams): Promise<TResponse> | TResponse;
}
```

▪️ On type **l'interface** afin que tout les **Usecases** partant de cette **interface** suivent le même modèle

▪️Dans le usecase 'CreateUser'
```typescript
import {Usecase} from "../domain/models/Usecase";
import {User} from "../domain/entities/User";
import {UserRepository} from "../domain/repositories/UserRepository";

interface CreateUserCommand {
    firstname: string;
    lastname: string;
}

//<CreateUserCommand, User>
//Ici je donne le typage de type "Typage Generique".
//Dans mon interface Usecase, sur le CreateUser,
//Je connais les Parametre de la fonction execute
//Je connais le retour de la fonction execute


export class CreateUser implements Usecase<CreateUserCommand, User> {

    constructor(
        private readonly _userRepository: UserRepository,
    ) {}

//J'implemente la methode execute car l'interface me l'impose.
//Je créé une interface pour m'assurer que tout les Usecase ont la méthode execute.

    execute(request: CreateUserCommand): User {
        const user = new User({
            firstname: request.firstname,
            lastname: request.lastname
        })
        return this._userRepository.create(user);
    }
}

```
-------------

# 💠 Les opérations CRUD

▪️ **CRUD** est un acronyme qui signifie :
  - **C** : _Create_, je suis dans l'action de créér.
  - **R** : _Read_, je suis dans l'action de lire.
  - **U** : _Update_, je suis dans l'action de mettre à jour.
  - **D** : _Delete_, je suis dans l'action de supprimer.
  
  
-------------

## 💠 Notes coraline 

▪️ dossier **user** : 
- interface avec les propriété
- export la classe

▪️ dossier **userrepository** : 
- importer user
- interface pour repertorier toute les methodes qui vont utiliser user

▪️ dossier **inmemoryuserrepository** : 
- importer ton repository
- importer user
- déclarer les methodes

▪️ fichier **usecase.ts** :
- interface modele pour faire le code

▪️ fichier **usercases** :
- import user repository et inmemory
- créer 2 interfaces param et response
- export class implement usecase params + response
- constructor à typer avec le repository
- executer requete avec params et response

-----------

# 💠 Base CleanArchi

▪️ Dossier Domain > Entities > Pizza.ts

- Ici on modélise une entité, c'est le centre du projet

- Pure language **Javascript**

📚 Exp:
```typescript
export enum PizzaSize {
  SMALL = 'SMALL',
  MEDIUM = 'MEDIUM',
  LARGE = 'LARGE',
}

export interface PizzaProperties {
  name: string;
  price: number;
  recipes: string[];
  size: PizzaSize;
  img: string;
}

export  class Pizza {
   props: PizzaProperties;

   constructor((props: PizzaProperties)) {
      this.props = props;
}
}
```

▪️ Créer ses **UseCases** en fonction des **user stories** dans un dossier **Usecases**

▪️ Créer un dossier **models** pour le typage
- Créer un fichier Usecase.ts pour modéliser les usecases

▪️ Créer son usecase

> **Tire toujours son code du domain**

📚 Exp:
```typescript
import  {Pizza} from "../domain/entitie/pizza";

export class CreatePizza implements Usecase<Pizza, Pizza> {}



```

▪️ Créer un fichier Repository.ts dans lequel on va créer le fichier PizzaRepository.ts

> C'est un repertoire qui va récuperer toute les pizzas créer etc


📚 Exp:
```typescript
import {Pizza} from "../Pizza";

export interface PizzaRepository {
  create(pizza: Pizza): Promise<Pizza> {
  
  export interface PizzaRepository {
     create(pizza: Pizza): Promise<Pizza>;
}
```

▪️Créer un dossier **/adapters/repository**
- Créer un fichier **InMemoryRepository**

> C'est la couche ou on va utiliser ce qu'on a modéliser dans le domain

```typescript
export class InmemoryRepository implements PizzaRepository {

   constructor() {}

}

  async create(pizza: Pizza): Promise<Pizza>;
```





