# 💠 Les bases Bootstrap

## 💠 Sommaire

1️⃣ [Intro](#introBootstrap)
2️⃣ [Les grids](#grid)
3️⃣ [Les components](#components)
4️⃣ [Le responsive](#responsive)

## 💠 Intro

<a id="introBootstrap"></a>
• C'est un framework utile à la création du designe de sites.
C'est un ensemble qui contient des codes HTML et CSS, des formulaires, boutons, outils de navigation et autres éléments interactifs, ainsi que des extensions JavaScript en option.

===========================
- Créer un nouveau projet
- Créer un fichier CSS et HTML
- Copier les liens bootstraps : 
  - Css : (<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">)
  - JS separate : (<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>)
                  (<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>)

===========================
<a id="grid"></a>

## 💠 Les grids

> Pour utiliser ce système de grilles, il faut définir un élément conteneur dans notre page HTML:
 - exp : `<div class="container"></div> ou  <div class="container-fluid"></div> `

> Placer une row (ligne) dans le container :
 - exp : ` <div class="container"><div class="row></div></div>`

> Placer autant de colomnes que voulu dans la row : 
 - exp : `<div class="container"><div class="row"><div class= col-5></div></div></div>`

> Bootstrap ne peut que contenir 12 col donc il faut sizer ses col sur 12
 - exp : ` <div class= col-9></div> `
         ` <div class= col-3></div> `
         
**La propriété offset permet de décaler du bord**

![img.png](assets/grid.png)

-------
<a id="Component"></a>

## 💠 Les components 

• Ce sont des éléments préfait disponible sur Bootstrap, qui en les copiant/collant dans le html on peut utiliser directement.
• On peut les styliser dans le CSS ou directement sur le HTML

-------
<a id="Responsive"></a>

## 💠 Le responsive 

> Le responsive permet de régler la taille des élements du site pour qu'ils s'adaptent à la taille de l'écran (smartphone,tablette...Ë)

• Breakpoints :
 - Small devices (landscape phones, 576px and up)
@media (min-width: 576px) { ... }

- Medium devices (tablets, 768px and up)
@media (min-width: 768px) { ... }

- Large devices (desktops, 992px and up)
@media (min-width: 992px) { ... }

- Extra large devices (large desktops, 1200px and up)
@media (min-width: 1200px) { ... }

