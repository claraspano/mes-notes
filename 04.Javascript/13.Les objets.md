![img.png](00.assets/javascript.png)

# 💠 Les objets 

## 🔗 Liens des videos:

1️⃣ Déclarer un objet : (https://youtu.be/1LdsB9NQAMQ)
2️⃣ Accéder à l’objet courant dans une méthode : (https://youtu.be/tx6Nlq9nZ5M)

--------

# 💠 Les objets

▪️ Un **objet** est un ensemble de **propriétés** et une **propriété** est une association entre une **key** et une **valeur**. La valeur d'une **propriété** peut être une **fonction**, auquel cas la **propriété** peut être appelée **« méthode »**. 

▪️ Les objets contiennent trois choses :
   - un constructeur
   - des propriétés
   - des méthodes.

## 💠 Méthodes : 

▪️ **Object.keys()** : Permet de retourner toutes les propriétés d'un objet sous forme d'un tableaux
____

# 💠 Déclarer un objet

📚 Exp:
````javascript

// { key : value}  
let phone = {
// Propriétés
  name: 'Iphone',
  marque: 'Apple',
  largeur: 5,
  longueur: 5,
};
        
// Méthodes
call: function(){
 console.log('Call Sam');
},

sendMsg : function(to) {
 console.log('Send sms to ' + to);
}        
        
//Séparer les propriétés, méthodes par des virgules
        
// Accéder à des propriétés 
console.log(phone.marque);
      
// Accéder à des méthodes
phone.call();
phone.sendMsg('Sam');    
     
````

▪ ️Il est possible d'ajouter une propriété à un objet défini précédemment avec cette syntaxe:

📚 Exp:
````javascript
phone.modele = 'Iphone'
````

--------

# 💠 Accéder à l'objet courant dans une méthode 

• **This** : Fait référence à l'objet courant, l'objet dans lequel la fonction est appelé

📚 Exp:
```javascript
let student = {
   name: 'Sam',
   class: 'Terminal',

say: function (say){
//"NAME: BONJOUR"
 console.log(this);
//Affiche l'objet
 console.log(this.name)
//Affiche la propriété de l'objet  
}
};

     student.say('Bonjour');
     student.say('Bonjour ' + student.name);
```

