![img.png](00.assets/mysql.png)

# 💠 My Sql

## 🔗 Liens des vidéos

1️⃣ Supprimer des données delete from : (https://youtu.be/vcXVZBIuRGs)

--------------

# 💠 Supprimer des données delete from

**▪ Supprimer via un ID️**

![img.png](00.assets/supp1.png)

- On supprime la derniere ligne

**▪️ Supprimer un élément avec un prix supérieur à 400**

![img.png](00.assets/supp2.png)

- On supprime une ligne grâce à une condition

**▪️ Pour ajouter une autre condition

![img.png](00.assets/supp3.png)

- Ajouter une autre condition utiliser le mot clé AND
- On veut supprimer un produit avec un prix ttc > a 400 et < 200 en ht et crée en 2022 
