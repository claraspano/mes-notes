![img.png](00.assets/Angular.png)

# 💠 Set up

## 🔗 Lien de la video

1️⃣ White rabbit (https://www.youtube.com/watch?v=Hhg5iKRv9pw&feature=youtu.be)

-------

## 💠 Installer _Angular_

▪️ Aller sur le site : https://angular.io/guide/setup-local

▪ Créer un nvx projet webstorm

▪ Inserer dans le terminal la commande : **npm install -g @angular/cli**

--------

## 💠 Creer un nouveau projet _Angular_

▪ Inserer dans le terminal la commande : **ng new my-app**

▪ Choisir les paramètres avec y/N

▪ Inserer la commande : **cd my-app** pour rentrer dans le dossier créer

▪ Inserer la commande : **ng serve** pour lancer le serveur

---------

## 💠 Afficher toute les commandes d'_Angular_

▪ Ecrire dans le terminal la commande : **ng**

---------




