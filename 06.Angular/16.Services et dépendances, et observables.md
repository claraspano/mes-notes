![img.png](00.assets/Angular.png)

# 💠 Services et injection de dépendances

## 🔗 Lien de la video

1️⃣ Services et injection de dépendances (https://www.youtube.com/watch?v=Vkp6_ZrF0W4&feature=youtu.be)

## 🔗 Liens utiles 

1️⃣ Doc Angular.io : (https://angular.io/guide/dependency-injection)
2️⃣ Doc : (https://cdiese.fr/angular-depency-injection/)
3️⃣ Doc Angular : (https://angular.io/guide/architecture-services)

-------------


# 💠 Qu'est ce qu'un service

▪️ Un **service** c'est une **classe** qui peut être réutilisées dans plusieurs **composants**
   
▪️Un **service** peut manager des données, pour rendre un code utilisable à plusieurs **composants**, ou faire des appels **serveur** (http)

▪️Le **service** est une vaste catégorie englobant toute **valeur, fonction ou fonctionnalité** dont une application a besoin. Un **service** est généralement _une classe avec un objectif étroit et bien défini_.

▪️**Angular** distingue les **composants** des **services** pour augmenter la _modularité_ et la _réutilisabilité_. En séparant les fonctionnalités liées à la vue d'un **composant** des autres types de traitement, vous pouvez rendre vos **classes** de composants allégées et efficaces.

![img.png](00.assets/services.png)

------------

# 💠 Créer un service injectable

▪️ Utiliser la CLI suivante :
````
ng generate service users
````

▪️ Va génerer un fichier test + un fichier service

📚 Exp:

▪️ Dans le fichier users.service.ts
```typescript
import { Injectable } from '@angular/core';
//

@Injectable({
//Un injecteur crée des dépendances et gère un conteneur d'instances de dépendances qu'il réutilise si possible
  providedIn: 'root'
//Le fournisseur
})

export class UsersService {
//Classe exportée et exportable

   constructor() {}
}
```

▪️ Dans le fichier users.component.ts
````typescript
constructor(private usersService: UsersService){
//La rend accessible partout et peut se partager
}
````

----------

# 💠 Injection de dépendance (DI)

▪️**DI** est câblé dans le framework _Angular_ et utilisé partout pour fournir de nouveaux **composants** avec les **services** ou autres éléments dont ils ont besoin. Les **composants** consomment des **services**; autrement dit, vous pouvez injecter un **service** dans un **composant**, en donnant au **composant** l'accès à cette **classe de service**.

▪️Les **dépendances** sont des **services** ou des **objets** dont une **classe** a besoin pour exécuter sa fonction. L'injection de **dépendances**, est _un modèle de conception_ dans lequel une **classe** demande des **dépendances** à des _sources externes_ plutôt que de les créer.

▪️Le cadre **DI** d'_Angular_ fournit des **dépendances** à une **classe** lors de _l'instanciation_. Vous pouvez utiliser **Angular DI** pour augmenter la _flexibilité et la modularité_ de vos applications.

------------

