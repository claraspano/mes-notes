![img.png](00.assets/Angular.png)

# 💠 Two way data binding

## 🔗 Lien de la video

1️⃣ White rabbit (https://www.youtube.com/watch?v=dqIo328Rp2s&feature=youtu.be)

## 🔗 Liens utiles

1️⃣ Doc Angular.io :(https://angular.io/guide/two-way-binding)

-------

🔸 C'est la liaison des données à double sens

> ▪️ La _liaison bidirectionnelle_ donne aux **composants** de votre application un moyen de partager des données. Utilisez la _liaison bidirectionnelle_ pour écouter les **événements** et mettre à jour les valeurs simultanément entre les **composants** parents et enfants

▪️ _La liaison bidirectionnelle_ combine la liaison de **propriété** avec la liaison **d'événement**:
  - La liaison de propriété définit une propriété d'élément spécifique **(property biding)**
  - La liaison d'événement écoute un événement de changement d'élément **(event binding)**
  
📚 Exp:

▪️ Dans le fichier HTML

```html
<input [(ngModel)]="text" type="text">

<p [style.color]="text"> {{ Text }} </p>
```

▪ Dans le fichier App.module.ts

```typescript
import {FormsModule} from '@angular/forms';

@ngModule({
imports: [
  FormsModule
],
})
//Importer le module dans le fichier app.modules.ts
```

▪ Dans le fichier TS

```typescript
export class AppComponent {
  text: string = 'red';
}
```
