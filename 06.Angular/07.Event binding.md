![img.png](00.assets/Angular.png)

# 💠 Event binding

## 🔗 Lien de la video

1️⃣ White rabbit (https://www.youtube.com/watch?v=27ZaLRwBsOc&feature=youtu.be)

## 🔗 Liens utiles

1️⃣ Doc Angular.io : (https://angular.io/guide/event-binding)

-------

## 💠 Lier des évenements

🔸 La **liaison d'événement** vous permet _d'écouter_ et de _répondre_ aux actions de l'utilisateur telles que les frappes, les mouvements de souris, les clics et les touches.

🖋 Syntaxe : 
```javascript
<button (click)="onSave()">Save</button>
```

![img.png](00.assets/enventbinding.png)

🔸 Sert à lier des éléments **HTML** à notre composant, on déclare des fonctions dans le fichier **TS**, dans le fichier **HTML** on déclare un évenement et comment on veut y réagir 

📚 Exp:

▪️ Dans le fichier HTML

```html
<button (click)="changeColor()">Click</button>
```

▪ Dans le fichier TS
```typescript
public changeColor() {
  this.title = "Event binding cliqué"
}
//La fonction liée au HTML va permettre de changer le texte du titre
```

📚 Exp:

▪️ Dans le fichier HTML

```html
<input #inputRef (change)="setText(inputRef)" type="text">
//Lie l'input au fichier TS grâce à la fonction setText

<p>({ text })</p>
//Va afficher le texte de l'input grâce à la fonction lié à l'evenement
```
// Créer un évenement et le lie à la fonction TS

▪ Dans le fichier TS
```typescript
public setText(input) {
  this.text = input.value;
}
```
//Fonction qui liée au HTML affiche le text de l'input


--------
## 💠 Récuperer les données d'un évenement :

🖋 Syntaxe
````javascript
$event
````

📚 Exp:

▪️ Dans le fichier HTML

````html
<button (click)="changeColor($event)">Click</button>
````

▪ Dans le fichier TS

````typescript
public changeColor(event) {
  this.title = "Event binding cliqué";
  console.log(event);
//Va afficher dans la console les données de l'évènement indiqué dans le HTML
}
````
----




