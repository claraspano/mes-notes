![img.png](assets/typescript.png)

# 💠 Les classes

## 🔗 Lien de la video

1️⃣ White rabbit (https://www.youtube.com/watch?v=u3FkhyrHJCs&feature=youtu.be)

## 🔗 Liens utiles

• http://pierreterrat.com/typescript-introduction-aux-classes/

• (https://www.youtube.com/watch?v=7EIkLSZJcSU&list=PLuWyq_EO5_AIxwo7mMdhbGQHp4uXse17T&index=16)

-------

## 💠 Les classes

> C'est une structure qui permet de définir un **objet**, va permettre _d'implémenter_ des **objets** et créer des instances de cet **objet**

🛑 On utilise jamais les **classes** pour _typer_ on préferera utiliser les **interfaces**

📚 Exp:
```typescript
//Plan de l'objet 
class UserProfile {
 firstname: string;
 nickname: string;
 age: number;

constructor(firstName: string, nickname: string, age: number) {
  this.firstname = firstName;
  this.nickname = nickname;
  this.age = age;
  }
}

const user1 = new UserProfile('Clara', 'Spano', 24);
console.log(user1.firstname); 
// Instance d'un objet basée sur les caractéristiques de la classe 'UserProfile'
```

## 💠 Public, protected, private

• Comme dans tous les _langages objets_, nous retrouvons en _TypeScript_ des **accesseurs** qui déterminent l’accessibilité des propriétés d’une **classe**.
  
• Si un **accesseur** n’est pas spécifié, alors **public** est défini par défaut.

📚 Exp:
```typescript
class FooBase {
    public x: number;
    private y: number;
    protected z: number;
}

// EFFECT ON INSTANCES
var foo = new FooBase();
foo.x; // okay
foo.y; // ERROR : private
foo.z; // ERROR : protected

// EFFECT ON CHILD CLASSES
class FooChild extends FooBase {
    constructor() {
      super();
        this.x; // okay
        this.y; // ERROR: private
        this.z; // okay
    }
}
```