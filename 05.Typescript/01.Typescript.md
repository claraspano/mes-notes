![img.png](assets/typescript.png)

# 💠 Typescript

• **Typescript** va rajouter un ensemble de fonctionnalitées à **javascript**

• C'est un _language de programmation_ qui va améliorer le code, la lisibilité et la sécurité

> • On développe en typescript et on transpile en javascript
 
## 💠 Sommaire

## 🔗 Liens des vidéos

01.Presentation nodejs (https://youtu.be/8Ak786GELow)
02.Presentation npm (https://youtu.be/N-RpiBQF4Zg)
03.Présentation typescript (https://youtu.be/a114c2kEgLU)
04.Configurer un package nodejs (https://youtu.be/s3WTu1jJgIc)
05.Basic types (https://youtu.be/WlyIWGLzHo4)
06.Functions (https://youtu.be/0iGLn7h73Hs)
07.Enums (https://youtu.be/o__NKdLOq8g)
08.Tuples (https://youtu.be/_p4Jn5tDNSU)
09.Literal types (https://youtu.be/8pexkpGCe5A)
10.Types assertions (https://youtu.be/Osl7mcX0jbs)
11.Interfaces (https://youtu.be/fJQZGKtMGTw)
12.Class (https://youtu.be/u3FkhyrHJCs)
13.Parameter properties (https://youtu.be/iwC6cAd2y4U)
14.Import export modules (https://youtu.be/u6hSND2wNcw)