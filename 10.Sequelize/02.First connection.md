![img.png](00.assets/sequelize.png)

# 💠 Sequelize

## 🔗 Liens des videos :

1️⃣ First connection : (https://www.youtube.com/watch?v=XO12rXPA5Pw)

---------------

# 💠 First connection

````javascript
// Ficher server.js

const express = require ('express'); // on insere le serveur http avec express
const cors = require('cors');

// Importer la dépendance Sequelize
const {Sequelize} = require('sequelize');

// Crée la connexion a MySQL
const sequelize = new Sequelize('app', 'root', 'Mettre son mdp', { // en premier parametre on met la base de donnée, en second l'utilisateur avec lequel je vais me connecter, et en 3ieme le mdp
    // option
    host: '127.0.0.1',
    dialect: "mysql"
});

// Tester la connexion
sequelize.authenticate().then(() => {// on utilise la constante sequelize et on utilise la méthode authenticate qui renvoie une promesse, et on reagit a la promesse avec then et une fonction callback

    console.log('connexion succesfull');
}).catch((error) => { // on réagit à l'erreur si ya une erreur
    console.error('echec connection', error)
});
````

▪️ Ensuite dans le terminal entrer la commande pour lancer la connection
````
node server.js
````
